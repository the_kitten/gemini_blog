# relay logic

```

          ████                                    ▄▄▄▄
          ████                                    ███▌
          ████                                    ███▌
          █████▄▄▄▄;                              ███▌
          ╨███████████▄▄       ▓████▌             ███▌
                  ╙▀█████▄     █████▌             ███▌
                      ▀███µ    █████▌             ███▌
                       ███▌    █████▌             ███▌
                     ▄████     █████▌             ███▌
              ,,▄▄▄█████▀      █████▌             ███▌
          4███████████▀        █████▌             ███▌
           ▀▀▀▀▀▀██████▄,      █████▌             ███▌
                    ▀████▌     █████▌           ▄▄████▄µ
                      ╙███▌    █████▌         ███████████▄
                      ,███▌    █████▌        ████└    ▀███▌
                    ▄▄████     █████▌       ▐███H      ████      ▄█▄▄
           ▄▄▄▄▄▄██████▀└      █████▌        ████▄   ,▄███▌    ,████▀
          ▐███████████▄        █████▌         ▀██████████▀    ▄████
              `└╙▀▀██████      █████▌           ╙▀▀▀▀▀▀`     ████▀
                     "████µ    █████▌                      ▄████└
                       ███▌    █████▌                     ▄███▀
                      ▄███▌    █████▌                   ,████┘
                 ,▄▄█████▀     █████▌                  ▄███▀
          ╒███████████▀╙       █████▌           ▄▄▄▄▄▄████▀
          `▀███████████▄       █████▌         ▄██████████▄
                   ╙▀████▄     █████▌        ████▀   `▀███▄
                      ▀███▌    █████▌       ▐███H      ████
                       ███▌    █████▌        ████;    ▄███▌
                    ,▄████     █████▌         ▀██████████▀
           ,▄▄▄▄▄▄██████▀      █████▌           ▀▀████▀└
          ▐███████████│        █████▌             ███▌
           ²╙╙▀▀▀▀██████▄      █████▌             ███▌
                    `▀████     █████▌             ███▌
                       ███▌    █████▌             ███▌
                      ▄███▌    █████▌             ███▌
                   ▄▄████▀     █████▌             ███▌
          ,▄███████████▀       █████▌             ███▌
          ███████▀▀▀'                             ███▌
          ████                                    ███▌
          ████                                    ███▌
          ████                                    ▀▀▀▌

```

A relay is usually an electromagnet that moves a metal part out of it deafault state when activated. This metal part will connect or not the circuit depending on its position. This way the electro magnet's circuit can close or open another circuit or another part of the same circuit depending on the usage.

In relay logic, we use two kinds of relays to create logic gates. Logic gates are operators working electronically. There is the NO (normally open) and the NC (normally closed). Those names are referring the states that the relays have when they are not activated. An open state means that the there is no connection, so the electricity will not pass. A close state means that there is a connection and the electricity is passed if there is on the wire. I just realised that the version I know and show here is called ladder logic. It is a version that can also be implemented in a automate that will simulate a relay circuit instead of doing it physically.

## symbols

For the sake of making the most of its text base, I will try to show everything using ASCII art. This way you can learn everything from a terminal. :)
I am not sure yet that I can do everything like that yet. Let's see.

 We represent NO relays like this:

```

 ----| |----

```

 And NC like this:

```

 ----|/|----

```

Relays can be connected to the same input or different inputs in one circuit. There are also buttons. They also can be normally open or normally close.

 We represent NO buttons like this:

```
      |
    -----
----o   o----

```
And NC like this:
```

----o | o----
    -----

```

The output is shown as a light or a circle.

## logic gates

Here is a list of logic gates. Keep in mind that a button is to be used by humans, but relays are buttons for circuits so buttons and relays can be used in all the logic gate circuits depanding of the need. For simplicity, we will use relay symbols here.

On the right, we have steady renting as a TRUE up and 0V as FALSE down.

R1 and R2 are the inputs here.

### BUFFER
```
TRUE
|
|    R1     out
|----| |----O----|

```

### NOT
```
TRUE
|
|    R1     out
|----|/|----O----|

```


### AND

```

|
|    R1     R2     out
|----| |----| |----O----|

```


### OR

```

|
|    R1         out
|----| |--------O----|
|    R2     |
|----| |----|

```

### XOR

```

|
|    R1     R2         out
|----| |----|/|--------O----|
|    R2     R1     |
|----| |----|/|----|

```

### NAND

```

|
|    R1         out
|----|/|--------O----|
|    R2     |
|----|/|----|

```

### NOR

```

|
|    R1     R2     out
|----|/|----|/|----O----|

```

### NXOR

```

|
|    R1     R2         out
|----| |----| |--------O----|
|    R2     R1     |
|----|/|----|/|----|

```

=> gemini://koyu.space/the_kitten/logic.gmi back_to_logic_main_page
=> gemini://koyu.space/the_kitten back_to_root_page