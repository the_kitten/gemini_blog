```
 ▄▄▄▄▄▄▄▄▄▄▄  ▄         ▄  ▄▄▄▄▄▄▄▄▄▄▄       ▄    ▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄        ▄ 
▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌     ▐░▌  ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░▌      ▐░▌
 ▀▀▀▀█░█▀▀▀▀ ▐░▌       ▐░▌▐░█▀▀▀▀▀▀▀▀▀      ▐░▌ ▐░▌  ▀▀▀▀█░█▀▀▀▀  ▀▀▀▀█░█▀▀▀▀  ▀▀▀▀█░█▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀ ▐░▌░▌     ▐░▌
     ▐░▌     ▐░▌       ▐░▌▐░▌               ▐░▌▐░▌       ▐░▌          ▐░▌          ▐░▌     ▐░▌          ▐░▌▐░▌    ▐░▌
     ▐░▌     ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄      ▐░▌░▌        ▐░▌          ▐░▌          ▐░▌     ▐░█▄▄▄▄▄▄▄▄▄ ▐░▌ ▐░▌   ▐░▌
     ▐░▌     ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌     ▐░░▌         ▐░▌          ▐░▌          ▐░▌     ▐░░░░░░░░░░░▌▐░▌  ▐░▌  ▐░▌
     ▐░▌     ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀      ▐░▌░▌        ▐░▌          ▐░▌          ▐░▌     ▐░█▀▀▀▀▀▀▀▀▀ ▐░▌   ▐░▌ ▐░▌
     ▐░▌     ▐░▌       ▐░▌▐░▌               ▐░▌▐░▌       ▐░▌          ▐░▌          ▐░▌     ▐░▌          ▐░▌    ▐░▌▐░▌
     ▐░▌     ▐░▌       ▐░▌▐░█▄▄▄▄▄▄▄▄▄      ▐░▌ ▐░▌  ▄▄▄▄█░█▄▄▄▄      ▐░▌          ▐░▌     ▐░█▄▄▄▄▄▄▄▄▄ ▐░▌     ▐░▐░▌
     ▐░▌     ▐░▌       ▐░▌▐░░░░░░░░░░░▌     ▐░▌  ▐░▌▐░░░░░░░░░░░▌     ▐░▌          ▐░▌     ▐░░░░░░░░░░░▌▐░▌      ▐░░▌
      ▀       ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀       ▀    ▀  ▀▀▀▀▀▀▀▀▀▀▀       ▀            ▀       ▀▀▀▀▀▀▀▀▀▀▀  ▀        ▀▀ 
                                                                                                                                                                                                                                         
```
# Content
I will use this site as some kind of blog. Just writing about what I want in the moment. Some will be a personal experience. Some will be about computer science, hacking, electronics or project ideas. Some will be about sexuality and gender.
I am a none binary geeky computer science student with a need for a space to express myself without restriction.

Love you all.

See you on the next article.

# Privacy tips
I am a privacy and cybersecurity enthusiast. Here some things that I do or not do yet myself to protect my privacy.
=> gemini://koyu.space/the_kitten/gemini_using_tor.gmi How_to_use_tor_with_gemini

# Logic
I had logic courses for both electronic and computer science. Both have their differences, but are made to picture the same reality. I will make an effort here to generalise it and make the links. In all courses, they were just using the word logic, but to differentiate the paradigms I will name them since I didn't find official names for it. I can divide it into three categories. In electronics, there are the logic gate paradigm and the, lets say electronic paradicme. In computer science, I sow the traditional paradigm. I call it traditional cause it is the same used by mathematicians. It is the oldest one. After doing each of them, I will make a generalisation. This part is more of a personal research I did by myself to make sense of all of it.

=> logic.gmi logic_small_courses_here

# Personal stories (NSFW stuff are here. 18+ here.)

=> stories.gmi stories

##Contact me
email: a.A_perfect.P_stranger@protonmail.com
